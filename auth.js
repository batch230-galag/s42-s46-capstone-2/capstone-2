// dependencies
const jwt = require("jsonwebtoken");
const secretKey = "E-commerceAPI"

// Token Creator
module.exports.createAccessToken = (user) => {
    const data =
    {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    return jwt.sign(data, secretKey);
}

// Token Verifier
module.exports.verifyToken = (request, response, next) => {
    let token = request.headers.authorization
    
    if(typeof token != "undefined") {
        // console.log(token)

        token = token.slice(7, token.length);
        return jwt.verify(token, secretKey, (error, data) => {
            if (error) {
                return response.send({
                    auth: "Failed."
                })
            } else {
                next();
            }
        })
    } else {
        return response.send("You need to login first")
    }
}

// Token Decoder
module.exports.decodeToken = (token) => {
    
    if (typeof token !== "undefined") {
        token = token.slice(7, token.length);
    }

    return jwt.verify(token, secretKey, (error, data) => {
        if (error) {
            return null;
        } else {
            return jwt.decode(token, {complete: true}).payload
        }
    })
}