// dependencies
const mongoose = require("mongoose");
const Product = require("../cap_models/products")
const auth = require("../auth");
const User = require("../cap_models/users")

// For Uploading Image
/* const path = require('path')
const multer = require('multer');

let storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/')
    },
    filename: function(req, file, cb) {
        let ext = path.extname(file.originalname)
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, Date.now() + ext)
    }
}) */


// s43
// Creating A product (Admin Only)
module.exports.addProduct = (req, res) => {

    const checkAdmin = auth.decodeToken(req.headers.authorization);

    Product.findOne({name: req.body.name})
    .then(result => {
        if (result != null && result.name == req.body.name) {
            return res.send("Duplicate Name")
        } else if (checkAdmin.isAdmin == true) {
            const url = req.protocol + '://' + req.get('host')
            let newProduct = new Product ({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price,
                stocks: req.body.stocks,
                foodType: req.body.foodType,
                productImage: req.body.productImage
                // image: url + '/public/' + req.file.filename
            })
            /* if(req.file) {
                newProduct.image = req.file.path
            } */
            // return res.send(newProduct)
            return newProduct.save()
            .then(result => res.send(result))
            .catch(error => res.send(error))
        } else {
            return res.send("You must be an ADMIN to add product");
        }
    })
}

// s43
// Getting All Active Products
module.exports.getAllActiveProducts = (req, res) => {
    Product.find({isActive: true})
    .then(result => {
        return res.send(result)
    })
}

// Getting All Products
module.exports.getAllProducts = (req, res) => {
    const access = auth.decodeToken(req.headers.authorization)
    
    Product.find()
    .then(result => {
            result.orders = undefined
            return res.send(result)
        } 
    )
}

// s44
// Updating A product
module.exports.updateProduct = (req, res) => {
    const checkAdmin = auth.decodeToken(req.headers.authorization);    
    let update = ({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        stocks: req.body.stocks,
        foodType: req.body.foodType
    })

    Product.findByIdAndUpdate(req.params.productId, update, {new:true})
    .then(result => {
        if(checkAdmin.isAdmin) {
            return res.send(result)
        } else {
            return res.send("Must be ADMIN")
        }
        
    })
    .catch(error => res.send(error))
}


// s44
// Archiving a Product
module.exports.archiveProduct = (req, res) => {
    const checkAdmin = auth.decodeToken(req.headers.authorization);
    let isActive = {
        isActive: req.body.isActive
    }
    
    Product.findByIdAndUpdate(req.params.productId, isActive, {new:true})
    .then(result => {
        if(checkAdmin.isAdmin == true) {
            return res.send(result)
        } else {
            return res.send("MUST BE AN ADMIN")
        }
    })
    .catch(error => res.send(error))
}

// s44
// Retrieving Single Product
module.exports.getSingleProduct = (req, res) => {


    Product.findById(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send(error))

}



// Clear All Products
module.exports.clearProducts = async (req, res) => {

    let user = await User.findById(req.body.userId)
    const product = await Product.findById(req.params.productId)

    const isExisting = product.orders.findIndex(prod => prod.userId == user._id)
    console.log(isExisting)

    if(isExisting >= 0) {
        product.orders.splice(isExisting, 1)
        // product.__v -= 1;
        product.save()
        return res.send(product)
    } else {
        return res.send("No User Id found")
    }
}