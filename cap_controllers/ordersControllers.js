// dependencies
const mongoose = require("mongoose");
const auth = require("../auth");
const bcrypt = require("bcrypt");

// Models
const User = require("../cap_models/users")
const Product = require("../cap_models/products");
const Order = require("../cap_models/orders");


module.exports.getUsersOrders = async (req, res) => {
    
    const access = auth.decodeToken(req.headers.authorization)
    const user = User.findById(access.id)

    let order = await Order.find({userId: access.id})
    .then(result => res.send(result))
}

module.exports.getAllOrders = async (req, res) => {
    
    const access = auth.decodeToken(req.headers.authorization)
    const user = User.findById(access.id)

    let order = await Order.find({})
    .then(result => res.send(result))
}

module.exports.isDelivered = (req, res) => {
    const checkAdmin = auth.decodeToken(req.headers.authorization);
    let isDelivered = {
        isDelivered: req.body.isDelivered
    }
    
    Order.findByIdAndUpdate(req.params.orderId, isDelivered, {new:true})
    .then(result => {
        if(checkAdmin.isAdmin == true) {
            /* let orderIndex = Order.findIndex(orderId => orderId._id)
            Order.splice(orderIndex, 1) */
            return res.send(result)
        } else {
            return res.send("MUST BE AN ADMIN")
        }
    })
    .catch(error => res.send(error))
}