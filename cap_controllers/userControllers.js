// dependencies
const mongoose = require("mongoose");
const auth = require("../auth");
const bcrypt = require("bcrypt");

// Models
const User = require("../cap_models/users")
const Product = require("../cap_models/products");
const Order = require("../cap_models/orders");

// s42
// User Registration
module.exports.register = (req, res) => {

    User.findOne({email: req.body.email})
    .then(result => {
        if (result != null && result.email == req.body.email) {
            return res.send(false) //If Email Already Exists
        } else {
            let newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 10),
                mobileNo: req.body.mobileNo
            })
            return newUser.save()
            .then(result => res.send(result))
            .catch(error => res.send(error)) 
        }
    })

}

// s42
// User Login
module.exports.login = (req, res) => {

    User.findOne({email: req.body.email})
    .then(result => {

        if(result == null || result.email != req.body.email) {
            // return res.send({false});
            return res.send(false);
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

            if(isPasswordCorrect) {
                return res.send({
                    access: auth.createAccessToken(result),
                    email: req.body.email,
                    id: result._id,
                    isAdmin: result.isAdmin
                })
            } else {
                return res.send(false)
                // return res.send("Email/Password is incorrect")
            }
        }
    })
}

// s45
// Create Order
/* module.exports.checkout = async (req, res) => {
    const access = auth.decodeToken(req.headers.authorization)
    
    let productName = await Product.findById(req.body.productId)
    .then(result => result.name)

    let stocksLeft = await Product.findById(req.body.productId)
    .then(result => result.stocks)
    
    let amount = await Product.findById(req.body.productId)
    .then(result => result.price)
    
    let newData = {
        userId: access.id,
        email: access.email,
        productId: req.body.productId,
        quantity: req.body.quantity,
        productName: productName,
        stocks: stocksLeft,
        price: amount
    }
    // res.send(newData)
    console.log(newData);
    
    // User
    let userUpdate = await User.findById(newData.userId)
    .then(user => {

        if(access.isAdmin == false) {

            user.orderedProduct.push({
                totalAmount: newData.price * newData.quantity,
                products: [{
                    productId: newData.productId,
                    productName: newData.productName,
                    quantity: newData.quantity
                }]
            })
    
    
            if(newData.quantity > newData.stocks ) {
                return false
            } else {
                // user.orderedProduct.totalAmount =newData.quantity;
                return user.save()
                .then(result => {
                    console.log(result)
                    return true
                })
                .catch(error => {
                    console.log(error)
                    return false
                })
            }
        } else {
            return res.send("You Cannot Purchase, You are an Admin")
        }

    })

    let orderId = await User.findById(newData.userId)
    .then(result => {
        return result.orderedProduct[result.orderedProduct.length -1]._id
    })
    

    // Products
    let productUpdate = await Product.findById(newData.productId)
    .then(product => {

        product.orders.push({
            orderId: orderId,
            userId: newData.userId,
            userEmail: newData.email,
            quantity: newData.quantity,

        })

        if(product.stocks < newData.quantity || product.stocks <= 0) {
            return false
        } else {
            product.stocks -= newData.quantity

            if(product.stocks == 0) {
                product.isActive = false;
            }
            return product.save()
            .then(result => {
                console.log(result)
                return true
            })
            .catch(error => {
                console.log(error)
                return false
            })
        }
    })

    console.log(orderId);
    console.log(userUpdate)
    console.log(productUpdate);

    if(userUpdate === false) {
        return res.status(404).send("No Available Stocks") 
    } else {
            return res.status(200).send(`Thank you for purchasing ${newData.productName}! (remaining stock: ${newData.stocks - newData.quantity}) \n Price per Product: ${newData.price} \n Total Amount: ${newData.price * newData.quantity}`)
    }


}
 */

// checkout V2
/* module.exports.checkout = async (req, res) => {
    const access = auth.decodeToken(req.headers.authorization)
    
    let productName = await Product.findById(req.body.productId)
    .then(result => result.name)

    let stocksLeft = await Product.findById(req.body.productId)
    .then(result => result.stocks)
    
    let amount = await Product.findById(req.body.productId)
    .then(result => result.price)
    
    let newData = {
        userId: access.id,
        email: access.email,
        productId: req.body.productId,
        quantity: req.body.quantity,
        productName: productName,
        stocks: stocksLeft,
        price: amount
    }
    // res.send(newData)
    console.log(newData);
    
    // User
    let userUpdate = await User.findById(newData.userId)
    .then(user => {

        if(access.isAdmin == false) {

            user.orderedProduct.push({
                totalAmount: newData.price * newData.quantity,
                products: [{
                    productId: newData.productId,
                    productName: newData.productName,
                    quantity: newData.quantity
                }]
            })
    
    
            if(newData.quantity > newData.stocks ) {
                return false
            } else {
                // user.orderedProduct.totalAmount =newData.quantity;
                return user.save()
                .then(result => {
                    console.log(result)
                    return true
                })
                .catch(error => {
                    console.log(error)
                    return false
                })
            }
        } else {
            return res.send("You Cannot Purchase, You are an Admin")
        }

    })

    let orderId = await User.findById(newData.userId)
    .then(result => {
        return result.orderedProduct[result.orderedProduct.length -1]._id
    })
    

    // Products
    let productUpdate = await Product.findById(newData.productId)
    .then(product => {

        product.orders.push({
            orderId: orderId,
            userId: newData.userId,
            userEmail: newData.email,
            quantity: newData.quantity,

        })

        if(product.stocks < newData.quantity || product.stocks <= 0) {
            return false
        } else {
            product.stocks -= newData.quantity

            if(product.stocks == 0) {
                product.isActive = false;
            }
            return product.save()
            .then(result => {
                console.log(result)
                return true
            })
            .catch(error => {
                console.log(error)
                return false
            })
        }
    })

    console.log(orderId);
    console.log(userUpdate)
    console.log(productUpdate);

    if(userUpdate === false) {
        return res.status(404).send("No Available Stocks") 
    } else {
            return res.status(200).send(`Thank you for purchasing ${newData.productName}! (remaining stock: ${newData.stocks - newData.quantity}) \n Price per Product: ${newData.price} \n Total Amount: ${newData.price * newData.quantity}`)
    }


} */

/* 
// checkout V3
module.exports.checkout = async (req, res) => {
    // 1. Find if there is accessToken
    const access = auth.decodeToken(req.headers.authorization)

    // 2. Send accessToken to User to show the result
    const user = await User.findById(access.id)

    const product = await Product.find({})
    // console.log(product)

    const order = await Order.findOne({totalPrice: req.body.totalPrice})
    // console.log(product)

    // to find all TotalAmount
    const totalAmountArray = user.orderedProduct.map(ta => ta.totalAmount)
    // console.log(totalAmountArray)

    const amountArray = totalAmountArray.length
    let total = 0
    
    for(let i = 0; i <amountArray; i++ ) {
        total += totalAmountArray[i];
    }
    let sum = total
    // console.log(sum)

    // Finding quantity per product and deduct it to the product.stock per id
    let prodQuantity = user.orderedProduct.map(prod => prod.products.map(quan => quan.quantity))
    // console.log(prodQuantity)

    const findNestedSum = (prodQuantity) => {
        let sum = 0;
        for(let i = 0; i <prodQuantity.length; i++) {
            sum += Array.isArray(prodQuantity[i]) ? findNestedSum(prodQuantity[i]) : prodQuantity[i];
        }
        return sum
    }
    const totalQuantity = findNestedSum(prodQuantity)
    // console.log(totalQuantity)

    // finding all products added to cart
    const allProducts = user.orderedProduct.flatMap(all => all.products)
    // console.log(allProducts)

    // Deducting the stocks from quantity
    // const updateItemStocks = (stocks, quantity) => stocks - quantity < 0 ? 0 : stocks - quantity;
    // const updateStoreStocks = (users, store) => {
    //         store.forEach((item, idx) => {
    //             const qty = allProducts[idx]?.quantity ?? 0;
    //             item.stocks = updateItemStocks(item.stocks, qty);
    //         });
    //     }
        
    // updateStoreStocks(user, product)
    // console.log(product)
    // const newProductArray = updateItemStocks(user, product)
    // console.log(newProductArray[1].name)

    // Deducting the stocks from quantity V2
    const updateCart = []
    const cartItems = await user

    cartItems.forEach(items => {
        updateCart.push({
            updateOne: {
                filter: {
                    _id: items.productId
                }
            }
        })
    })
    



    // last part
    if(access.id !== null){
        let newOrder = new Order({
            name: user.firstName,
            email: user.email,
            totalPrice: sum,
            orders: allProducts
        })
        // newOrder.save()
        // user.orderedProduct = []
        // user.save()
        return res.send(newOrder)
    }
} 
*/

// checkout V4
module.exports.checkout = async (req, res) => {
    // 1. Find if there is accessToken
    const access = auth.decodeToken(req.headers.authorization)
    const product = await Product.find({})
    
    const user2 = await User.findById(access.id)



    User.findById(access.id)
    .populate('orderedProduct.products.productId')
    .exec((err, user) => {
        if(err) {
            console.log("error")
        } else {
            user.orderedProduct.forEach(order => {
                order.products.forEach(item => {
                    const product = item.productId;
                    const quantityToDeduct = item.quantity;
                    Product.findByIdAndUpdate(
                        {_id : product},
                        { 
                            $inc: {stocks : -quantityToDeduct},
                            $set: {isActive: true },
                            $push: { orders: { userId: user._id, quantity: quantityToDeduct, userEmail: user.email } }
                        },
                        {new: true},
                        (err, product) => {
                            if(err) {
                                console.log("error")
                            }
                        }
                    )
                    
                })
            })
        }
        user.markModified('user._id');
        user.save()
    })


    // finding all products added to cart
    const allProducts = user2.orderedProduct.flatMap(all => all.products)
    // console.log(allProducts)

    // to find all TotalAmount
    const totalAmountArray = user2.orderedProduct.map(ta => ta.totalAmount)
    // console.log(totalAmountArray)

    const amountArray = totalAmountArray.length
    let total = 0
    
    for(let i = 0; i <amountArray; i++ ) {
        total += totalAmountArray[i];
    }
    let sum = total
    // console.log(sum)

    // Finding quantity per product and deduct it to the product.stock per id
    let prodQuantity = user2.orderedProduct.map(prod => prod.products.map(quan => quan.quantity))
    // console.log(prodQuantity)

    const findNestedSum = (prodQuantity) => {
        let sum = 0;
        for(let i = 0; i <prodQuantity.length; i++) {
            sum += Array.isArray(prodQuantity[i]) ? findNestedSum(prodQuantity[i]) : prodQuantity[i];
        }
        return sum
    }
    const totalQuantity = findNestedSum(prodQuantity)
    // console.log(totalQuantity)

    // To update Products orders
    // User.findById(access.id, (err, user) => {
    //     if (err) {
    //         console.log("error")
    //     } else {
    //         user.orderedProduct.forEach(order => {
    //             order.products.forEach(item => {
    //             const productId = item.productId; // Assuming productId is the name of the product ID field.
    //             const quantityOrdered = item.quantity;
    //             Product.updateMany(
    //                 { _id: productId }, // filter
    //                 {
                        
    //                     $push: { orders: { userId: user._id, quantity: quantityOrdered, userEmail: user.email } },
    //                     $inc: { stocks: -quantityOrdered },
    //                     $set: {isActive:(stocks - quantityOrdered) === 0 ? false : true}
    //                 },{new: true},
    //                 /* { 
    //                     $push: { orders: { userId: user._id, quantity: quantityOrdered } },
    //                     $inc: { stocks: -quantityOrdered },
    //                     {isActive: {$gt: 4} },
    //                     $set: { isActive: false } // Set active state to false if quantity is zero or less.
    //                 }, */
    //                 /* (err, result) => {
    //                 if (err) {
    //                     // Handle error.
    //                 } else {
    //                     // Orders array has been updated for all products that were ordered.
    //                 }
    //                 } */
    //             );
    //             });
    //         });
    //     }
    //   });


    if(access.id !== null){
        let newOrder = new Order({
            userId: user2.id,
            name: user2.firstName,
            email: user2.email,
            totalPrice: sum,
            orders: allProducts
        })
        newOrder.save()
        user2.orderedProduct = []
        user2.save()
        return res.send(newOrder)
    }
}

// s45
module.exports.getProfile = async (req, res) => {

    const access = auth.decodeToken(req.headers.authorization)
    
    let user = await User.findById(access.id)
    .then(result => {
        // result.password = null;
        // result.isAdmin = undefined;
        return res.send(result);
    })
}

/* STRETCH GOALS */
// Set User as Admin (Must be an Admin to change)
module.exports.setAdmin = (req, res) => {

    const checkAdmin = auth.decodeToken(req.headers.authorization)
    let setAdmin = ({
        isAdmin: req.body.isAdmin
    })

    if(checkAdmin.isAdmin == true) {
        User.findByIdAndUpdate(req.params.userId, setAdmin, {new: true})
        .then(result => res.send(result))
        .catch(error => res.send(error))
    } /* else {
        return res.send("Must be an Admin")
    } */
}

// Retrieve authenticated user’s orders
module.exports.userOrders = (req, res) => {
    // const access = auth.decodeToken(req.headers.authorization)

    User.findById(req.params.userId)
    .then(result => {
        return res.send(result.orderedProduct)
    })
}

/* Stretch Goals */
// retrieve all order (Admin only)
module.exports.allOrders = (req, res) => {

    const access = auth.decodeToken(req.headers.authorization)

    if(access.isAdmin) {
        Product.find({})
        .then(result => {
            
            return res.send(result)
        })
        .catch(error => res.send(error))
    } else {
        return res.send("Must be ADMIN")

    }
}

// add to cart ver.2
module.exports.addToCart = async (req, res) => {

    // Gets the user and product details
    const access = auth.decodeToken(req.headers.authorization)
    const user = await User.findById(access.id)
    const product = await Product.findById(req.params.pId)

    // For checking if the Product is already at the Cart
    const orderedArray = user.orderedProduct.findIndex(prod => prod.products.findIndex(pid => pid.productId === product.id)!= -1)
    console.log(orderedArray)
    
    if(access.isAdmin == false) {
        if(orderedArray >= 0) {
            // For finding what index is the product Id existing
            const productsArray = user.orderedProduct[orderedArray].products.findIndex(p => p.productId === product.id)
            console.log(productsArray)
            // Checks if the product is already on the Cart

                if(product.stocks < req.body.quantity) {
                    return res.send(false)
                } else {
                    let total = user.orderedProduct[orderedArray].totalAmount += (product.price * req.body.quantity)
                    console.log(total)
                    user.orderedProduct[orderedArray].products[productsArray].quantity += req.body.quantity

                    // product.stocks -= req.body.quantity
                    if(product.stocks == 0) {
                        product.isActive = false
                        // product.save()
                    }
                    user.save()
                    product.save()
                    }   
            } else {
                if(product.stocks < req.body.quantity) {
                    return res.send(false)
                } else {
                    user.orderedProduct.push({
                        totalAmount: product.price * req.body.quantity,
                        products: [{
                            productId: product.id,
                            productName: product.name,
                            quantity: req.body.quantity
                        }]
                    })
                    // product.stocks -= req.body.quantity
                    if(product.stocks == 0) {
                        product.isActive = false
                    }
                    user.save()
                    product.save()
                    return res.send(user)
                }
            }
            return res.send(user)
    } else {
        return res.send(false)
    }


}

// Check Cart
module.exports.checkCart = async (req, res) => {


    const access = auth.decodeToken(req.headers.authorization)

    const user = await User.findById(access.id)
    .then(result => res.send(result.orderedProduct))

}

// Delete OrderedProduct
module.exports.deleteFromCart = async (req, res) => {

    let access = auth.decodeToken(req.headers.authorization)
    let user = await User.findById(access.id)
    const product = await Product.findById(req.params.productId)
    
// Version 2
    // Finding the exact index of the product Id
        const orderedArray = user.orderedProduct.findIndex(prod => prod.products.findIndex(pid => pid.productId === product.id)!= -1)
        // console.log(orderedArray)
    
    // Checks if the product Id is existing
    if(access.isAdmin == false) {
        if(orderedArray >= 0) {
            // For finding what index is the product Id existing
            const productsArray = user.orderedProduct[orderedArray].products.findIndex(p => p.productId === product.id)
            // console.log(productsArray)
            
            const qty = user.orderedProduct[orderedArray].products[productsArray].quantity
                if(qty < req.body.quantity) {
                    return res.send(false)
                } else {
                    user.orderedProduct[orderedArray].totalAmount -= (product.price * req.body.quantity)
                    let q2 = user.orderedProduct[orderedArray].products[productsArray].quantity -= req.body.quantity

                        if(q2 === 0) {
                            user.orderedProduct.splice(orderedArray, 1)
                            user.save()
                            return res.send(user)
                        } 

                    // qty -= req.body.quantity
                    // product.stocks += req.body.quantity
                    if(product.stocks !== 0 ) {
                        product.isActive = true
                        // product.save()
                    }
                    
                    
                    user.save()
                    // product.save()
                } 
                
            }
            return res.send(user)
    } else {
        return res.send(false)
    }
}

module.exports.getAllUser = (req, res) => {
    User.find({}).then(result => res.send(result))
}

