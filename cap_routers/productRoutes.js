// dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productControllers = require("../cap_controllers/productControllers");

// For Uploading Image
const path = require('path')
const multer = require('multer');

let storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/')
    },
    filename: function(req, file, cb) {
        let ext = path.extname(file.originalname)
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, Date.now() + ext)
    }
})

let upload = multer ({
    storage: storage,
    fileFilter: function(req, file, callback) {
        if(
            file.mimetype == "image/png" ||
            file.mimetype == "image/jpg"
        ) {
            callback(null, true)
        } else {
            console.log("jpg and png file only")
            callback(null, false)
        }
    }
    
})


router.post("/addProduct", upload.single('productImage'), auth.verifyToken, productControllers.addProduct);
router.get("/allActiveProducts", /* auth.verifyToken, */ productControllers.getAllActiveProducts);
router.get("/allProducts", productControllers.getAllProducts);
router.get("/:productId", productControllers.getSingleProduct)

router.put("/:productId/update", auth.verifyToken, productControllers.updateProduct)
router.patch("/archive/:productId", auth.verifyToken, productControllers.archiveProduct)

router.delete("/:productId/clearProducts", productControllers.clearProducts)

/* Stretch Goals */


module.exports = router;