// dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const ordersControllers = require('../cap_controllers/ordersControllers')


router.get("/userOrders", auth.verifyToken, ordersControllers.getUsersOrders)

router.get("/allOrders", auth.verifyToken, ordersControllers.getAllOrders)

router.put("/isDelivered/:orderId", auth.verifyToken, ordersControllers.isDelivered)

module.exports = router;