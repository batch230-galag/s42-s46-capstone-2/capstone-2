// dependencies
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userControllers = require("../cap_controllers/userControllers")


router.post("/register", userControllers.register);
router.post("/login", userControllers.login);

router.get("/userDetails", auth.verifyToken, userControllers.getProfile)
router.post("/checkout", auth.verifyToken, userControllers.checkout)

// Stretch Goals
router.put("/setAdmin/:userId", auth.verifyToken, userControllers.setAdmin)
router.get("/allOrders", auth.verifyToken, userControllers.allOrders)
router.get("/:userId/orders", auth.verifyToken, userControllers.userOrders)

router.get("/checkCart", auth.verifyToken, userControllers.checkCart)
router.post("/addToCart/:pId", auth.verifyToken, userControllers.addToCart)
router.delete("/removeToCart/:productId", auth.verifyToken, userControllers.deleteFromCart)
router.get("/allUsers", userControllers.getAllUser)


module.exports = router;