// dependencies
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
    {
        firstName: {
            type: String,
            required: [true, "First Name is required"]
        },
        lastName: {
            type: String,
            required: [true, "Last Name is required"]
        },
        email: {
            type: String,
            required: [true, "Email is required"]
        },
        password: {
            type: String,
            required: [true, "Password is required"]
        },
        mobileNo: {
            type: String,
            required: [true, "Mobile Number is required"]
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        orderedProduct: [
            {
                totalAmount: {
                    type: Number,
                },
                purchasedOn: {
                    type: Date,
                    default: Date.now(),
                    get: function (purchasedOn) {
                        return `${purchasedOn.getDate()}/${purchasedOn.getMonth() + 1}/${purchasedOn.getFullYear()}`},
                    set: function (value) {
                        return new Date(value)}
                },
                products: [
                    {
                        productId: {
                            type: String
                        },
                        productName: {
                            type: String
                        },
                        quantity: {
                            type: Number
                        }
                    }
                ]
            }
        ]
    }
)

module.exports = mongoose.model("User", userSchema)