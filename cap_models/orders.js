// dependencies
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
    {
        userId: {
            type: String,
            required: [true, "Product Name is required"]
        },
        name: {
            type: String,
            required: [true, "Product Name is required"]
        },
        email: {
            type: String,
            required: [true, "Product Name is required"]
        },
        totalPrice: {
            type: Number,
            required: [true, "Price is required"]
        },
        purchasedOn: {
            type: Date,
            default: new Date()
        },
        isDelivered: {
            type: Boolean,
            default: false
        },
        orders: []
    }
)

orderSchema.virtual('created_at_formatted').get(function () {
    return `${this.created_at.getDate()}/${this.created_at.getMonth() + 1}/${this.created_at.getFullYear()}`
  });

module.exports = mongoose.model("Orders", orderSchema)
