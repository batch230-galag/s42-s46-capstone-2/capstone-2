// dependencies
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
    {
        /* image: {
            type: String
        }, */
        name: {
            type: String,
            required: [true, "Product Name is required"]
        },
        description: {
            type: String,
            required: [true, "Description is required"]
        },
        price: {
            type: Number,
            required: [true, "Price is required"]
        },
        stocks: {
            type: Number,
            required: [true, "Stocks is required"]
        },
        productImage: {
            type: String,
            required: [true, "Product Image is required"]
        },
        isActive: {
            type: Boolean,
            default: true
        },
        foodType: {
            type: String,
            enum: [
                'drinks',
                'biscuits',
                'chips',
                'candies'    
            ]
        },
        orders: [
            {
                orderId: {
                    type: String
                },
                userId: {
                    type: String
                },
                userEmail: {
                    type: String
                },
                quantity: {
                    type: Number
                },
                purchasedOn: {
                    type: Date,
                    default: new Date()
                }
            }
        ]
    }
)

module.exports = mongoose.model("Product", productSchema)