// dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// routes
const userRoutes = require("./cap_routers/userRoutes");
const productRoutes = require("./cap_routers/productRoutes");
const ordersRoutes = require("./cap_routers/ordersRoutes");


// create server/application
const app = express();

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
    // for making image folder public
app.use('/public', express.static('public'))

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", ordersRoutes);

// Connect to Mongoose
mongoose.connect("mongodb+srv://admin:admin@batch230.rr5eflw.mongodb.net/Galag_E-commerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Your E-commerce Database"));


// Don't forget this code below 
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000} `)
})